#define GLM_ENABLE_EXPERIMENTAL
#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include "Maze.h"
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp> 
#include "Camera.hpp"
#include <Texture.hpp>
#include <cstdlib>

#include <iostream>
#include <vector>

/**
Несколько примеров шейдеров
*/
class TextureSet {
public:
    TexturePtr color;
    TexturePtr normal;

    TextureSet() {}
    TextureSet(TexturePtr color, TexturePtr normal) : color(color), normal(normal) {}
    TextureSet(char* color, char* normal) : color(loadTexture(color)), normal(loadTexture(normal)) {}
    
    TexturePtr operator[](int index) {
        if (index == 1) {
            return normal;
        }
        return color;
    }
};

class MazeApp : public Application {
public:
    vector<MeshPtr> meshes;
    vector<CameraMoverPtr> movers = {std::make_shared<OrbitCameraMover>(), std::make_shared<FreeCameraMover>()};
    int currentIndex = 0;
    int spotlight = 0;
    glm::vec2 pos;
    glm::vec2 vel;
    Maze maze;
    TextureSet bricks_texture;
    TextureSet floor_texture;
    vector<GLuint> samplers;
    vector<MeshPtr> portals;
    vector<glm::vec3> portal_positions;
    vector<glm::vec3> portal_normals;
    int next_portal = 0;
    glm::vec3 portal_position;
    GLuint _depthTexId;
    GLuint _sampler;

    ShaderProgramPtr shader;
    ShaderProgramPtr stencil_shader;

    MazeApp() : maze(10),
        samplers(2),
        pos(0.0f, 0.0f),
        vel(0.0f, 0.0f),
        Application(nullptr) {
            _cameraMover = movers[0];
    }

    void makeScene() override
    {
        Application::makeScene();

        maze.generate();
        maze.debug();
        meshes = maze.get_meshes();
        meshes.push_back(makeFloor(0));
        for (MeshPtr mesh : meshes) {
            mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
        }

        //=========================================================

        shader = std::make_shared<ShaderProgram>("492ZinovData/spotlight.vert", "492ZinovData/spotlight.frag");
        stencil_shader = std::make_shared<ShaderProgram>("492ZinovData/shader.vert", "492ZinovData/shader.frag");
        bricks_texture = TextureSet("492ZinovData/bricks.jpg", "492ZinovData/bricks_norm.jpg");
        floor_texture = TextureSet("492ZinovData/floor.jpg", "492ZinovData/floor_norm.jpg");
        for (int i = 0; i < 2; ++i) {
            glGenSamplers(1, &(samplers[i]));
            glSamplerParameteri(samplers[i], GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glSamplerParameteri(samplers[i], GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glSamplerParameteri(samplers[i], GL_TEXTURE_WRAP_S, GL_REPEAT);
            glSamplerParameteri(samplers[i], GL_TEXTURE_WRAP_T, GL_REPEAT);
        }
        portals  = {makePortal(glm::vec3(0, 5, 0.5), glm::vec3(1, 0, 0)), makePortal(glm::vec3(5, 0, 0.5), glm::vec3(0, 1, 0))};
        portal_positions  = {glm::vec3(0, 5, 0.5), glm::vec3(5, 0, 0.5)};
        portal_normals  = {glm::vec3(1, 0, 0), glm::vec3(0, 1, 0)};
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glGenTextures(1, &_depthTexId);
        glBindTexture(GL_TEXTURE_2D, _depthTexId);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, 0);
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void checkUpdate() override
    {
        if (currentIndex == 1) {
            maze.clamp_pos(std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_pos);
        }
        for (int i = 0; i < 2; ++i) {
            glm::vec3 pos = std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_pos;
            glm::vec3 diff = pos - portal_positions[i];
            diff.z = 0;
            if (glm::length(diff) < 0.3) {
                glm::vec3 up(0.0f, 0.0f, 1.0f);
                auto angle = glm::orientedAngle(glm::normalize(portal_normals[i]), -glm::normalize(portal_normals[1 - i]), up);
                glm::vec3 new_diff = glm::rotateZ(diff, angle);
                std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_pos = portal_positions[1 - i] + portal_normals[1 - i] * 0.5f;
                std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_pos.z = pos.z;
                std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_rot *= glm::angleAxis(-angle, up);
                movers[1]->updateView(_window);
                break;
            }
        }
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("Maze", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            ImGui::RadioButton("orbit camera", &currentIndex, 0);
            ImGui::RadioButton("first person camera", &currentIndex, 1);
            ImGui::RadioButton("overhead light", &spotlight, 0);
            ImGui::RadioButton("spotlight", &spotlight, 1);
        }
        ImGui::End();
    }

    void draw_meshes(glm::vec3 color) {
        shader->use();
        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        if (spotlight) {
            shader->setVec3Uniform("light.pos", std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_pos);
            glm::vec3 light_dir = glm::normalize(glm::vec3(0.0f, 0.0f, -1.0f) * std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_rot);
            glm::vec3 up = glm::normalize(glm::cross(light_dir, glm::vec3(1.0f, -1.0f, 1.0f)));
            glm::vec3 right = glm::normalize(glm::cross(light_dir, up));
            pos += vel;
            vel -= pos * 0.005f;
            vel -= vel * 0.01f;
            vel += glm::vec2((rand() * 1.0 / RAND_MAX - 0.5), (rand() * 1.0 / RAND_MAX - 0.5)) * 0.05f;
            //pos = glm::vec2(10.0f, 0.0f);
            shader->setVec3Uniform("light.coneDirection", light_dir + up * pos[0] * 0.01f + right * pos[1] * 0.01f);
            shader->setFloatUniform("light.coneAngle", 30.0f);
            shader->setVec3Uniform("light.Ld", color);
        } else {
            shader->setVec3Uniform("light.pos", glm::vec3(5.0f, 5.0f, 20.0f));
            shader->setVec3Uniform("light.coneDirection", glm::vec3(0.0f, 0.0f, -1.0f));
            shader->setFloatUniform("light.coneAngle", 180.0f);
            shader->setVec3Uniform("light.Ld", 100.0f * color);
        }
        shader->setVec3Uniform("light.La", 0.1f * color);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        for (MeshPtr mesh : meshes) {
            auto texture_set = &bricks_texture;
            if (mesh->getMaterial()) {
                texture_set = &floor_texture;
            }
            for (int i = 0; i < 2; ++i) {
                if (USE_DSA) {
                    glBindTextureUnit(1 + i, (*texture_set)[i]->texture());
                    glBindSampler(1 + i, samplers[i]);
                }
                else {
                    glBindSampler(1 + i, samplers[i]);
                    glActiveTexture(GL_TEXTURE0 + 1 + i);  //текстурный юнит 0
                    (*texture_set)[i]->bind();
                }
            }
            shader->setIntUniform("diffuseTex", 1);
            shader->setIntUniform("normalTex", 2);
            shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
            shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * mesh->modelMatrix()))));

            shader->setVec3Uniform("material.Ka", glm::vec3(1.0f, 0.0f, 0.0f));
            shader->setVec3Uniform("material.Kd", glm::vec3(1.0f, 0.0f, 0.0f));
            mesh->draw();
        }
    }

    void draw() override
    {
        Application::draw();
        Application::setCameraMover(movers[currentIndex]);

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        shader->setFloatUniform("width", width);
        shader->setFloatUniform("height", height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glBindTexture(GL_TEXTURE_2D, _depthTexId);
        glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 0, 0, width, height, 0);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindTexture(GL_TEXTURE_2D, _depthTexId);
        glBindSampler(0, _sampler);
        shader->setIntUniform("tex", 0);


        draw_meshes(glm::vec3(1.0f, 1.0f, 1.0f));

        float zNear = 0.1;
        float zFar = 100.0;
        GLfloat depthSample;
        glReadPixels((int)round(width / 2), (int)round(height / 2), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depthSample);
        depthSample = 2.0 * depthSample - 1.0;
        float zLinear = 2.0 * zNear * zFar / (zFar + zNear - depthSample * (zFar - zNear));
        glm::vec3 pos = std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_pos;
        auto rot = std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_rot;
        glm::vec3 dir = glm::normalize(glm::vec3(0.0f, 0.0f, -1.0f) * rot);
        portal_position = pos + dir * zLinear;

        glBindTexture(GL_TEXTURE_2D, _depthTexId);
        glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 0, 0, width, height, 0);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindTexture(GL_TEXTURE_2D, _depthTexId);
        glBindSampler(0, _sampler);
        shader->setIntUniform("tex", 0);

        glEnable(GL_STENCIL_TEST);
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        glDepthMask(GL_FALSE);
        glStencilFunc(GL_ALWAYS, 0xFF, 0xFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
        for (int i = 0; i < 2; ++i) {
            glStencilMask(0x1 << i);
            stencil_shader->use();
            stencil_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
            stencil_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
            auto mesh = portals[i];
            stencil_shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
            mesh->draw();
        }

        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        glDepthMask(GL_TRUE);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        for (int i = 0; i < 2; ++i) {
            glClear(GL_DEPTH_BUFFER_BIT);
            glm::vec3 up(0.0f, 0.0f, 1.0f);
            glm::vec3 diff = pos - portal_positions[i];
            auto angle = glm::orientedAngle(glm::normalize(portal_normals[i]), -glm::normalize(portal_normals[1 - i]), up);
            glm::vec3 new_diff = glm::rotateZ(diff, angle);
            std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_pos = portal_positions[1 - i] + new_diff;
            std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_rot *= glm::angleAxis(-angle, up);
            movers[1]->updateView(_window);//, max(glm::length(diff) + 0.2, 0.1));
            _camera = movers[1]->cameraInfo();
            glStencilFunc(GL_EQUAL, 0xFF, 0x1 << i);
            draw_meshes(glm::vec3(0.5f, 1.0f, 1.0f));
            std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_pos = pos;
            std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_rot = rot;
            movers[1]->updateView(_window);
            _camera = movers[1]->cameraInfo();
        }
        glStencilMask(0xFF);
        glDisable(GL_STENCIL_TEST);
        glBindSampler(0, 0);
    }

    void handleKey(int key, int scancode, int action, int mods) {
        Application::handleKey(key, scancode, action, mods);
        if (key == GLFW_KEY_E && action == GLFW_PRESS) {
            GLfloat x = portal_position[0];
            glm::vec3 normal(0.0f, 1.0f, 0.0f);
            if (abs(portal_position[0] - round(portal_position[0])) < abs(portal_position[1] - round(portal_position[1]))) {
                normal = glm::vec3(1.0f, 0.0f, 0.0f);
            }
            auto pos = std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_pos;
            if (glm::dot(portal_position - pos, normal) > 0) {
                normal = -1.0f * normal;
            }
            auto dir = glm::normalize(glm::vec3(0.0f, 0.0f, -1.0f) * std::dynamic_pointer_cast<FreeCameraMover>(movers[1])->_rot);
            std::cerr << pos.x << ' ' << pos.y << ' ' << pos.z << std::endl;
            std::cerr << dir.x << ' ' << dir.y << ' ' << dir.z << std::endl;
            portals[next_portal] = makePortal(portal_position, normal);
            portals[next_portal]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
            portal_positions[next_portal] = portal_position;
            portal_normals[next_portal] = normal;
            std::cerr << portal_position.x << ' ' << portal_position.y << ' ' << portal_position.z << std::endl;
            std::cerr << normal.x << ' ' << normal.y << ' ' << normal.z << std::endl;
            next_portal = 1 - next_portal;
        }
    }
};

int main()
{
    MazeApp app;
    app.start();
    app.maze.debug();

    return 0;
}
